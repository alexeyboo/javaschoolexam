package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        Stack<Character> stack = new Stack<>();

        List in = doParseToList(statement);

        System.out.println(in);

        LinkedList postFix = new LinkedList();
        for (int i = 0; i < in.size(); i++) {
            char c;
            if (isOperator(in.get(i))) {
                c = in.get(i).toString().charAt(0);
                switch (c) {
                    case '+':
                    case '-':
                        getOperator(stack, postFix, c, 1);
                        break;
                    case '*':
                    case '/':
                        getOperator(stack, postFix, c, 2);
                        break;
                    case '(':
                        stack.push(c);
                        break;
                    case ')':
                        getParenthesis(stack, postFix);
                        break;
                }
            } else {
                postFix.add(in.get(i));
            }
        }

        while (!stack.isEmpty()) {
            postFix.add(stack.pop());
        }

        System.out.println(postFix);
        String out = doCalc(postFix);

        return out;
    }

    private boolean isOperator(Object o) {
        Set<Character> operators = new HashSet<>(Arrays.asList('*', '/', '+', '-', '(', ')'));
        String s = o.toString();
        if (s.length() == 1 && operators.contains(Character.valueOf(s.charAt(0)))) {
            return true;
        }
        return false;
    }

    private StringBuilder getLast(List<StringBuilder> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i) != null) {
                return list.get(i);
            }
        }
        return null;
    }

    private void getOperator(Stack<Character> stack, LinkedList out, char c, int prio1) {
        while (!stack.isEmpty()) {
            char top = stack.pop();
            if (top == '(') {
                stack.push(top);
                break;
            } else {
                int prio2 = (top == '+' || top == '-' ? 1 : 2);
                if (prio2 < prio1) {
                    stack.push(top);
                    break;
                } else {
                    out.add(top);
                }
            }
        }
        stack.push(c);
    }

    private void getParenthesis(Stack<Character> stack, LinkedList out) {
        while (!stack.isEmpty()) {
            char c = stack.pop();
            if (c == '(') {
                break;
            } else {
                out.add(c);
            }
        }
    }

    private String doCalc(LinkedList postFix) {
        Stack<Double> stack = new Stack<>();
        double d1, d2, between;
        char c;

        for (int i = 0; i < postFix.size(); i++) {
            if (!isOperator(postFix.get(i))) {
                try {
                    stack.push(Double.parseDouble(postFix.get(i).toString()));
                } catch (NumberFormatException e) {
                    return null;
                }

            } else {
                c = postFix.get(i).toString().charAt(0);
                try {
                    d2 = stack.pop();
                    d1 = stack.pop();
                } catch (EmptyStackException e) {
                    return null;
                }

                switch (c) {
                    case '+':
                        between = d1 + d2;
                        break;
                    case '-':
                        between = d1 - d2;
                        break;
                    case '*':
                        between = d1 * d2;
                        break;
                    case '/':
                        if (d2 != 0) {
                            between = d1 / d2;
                        } else {
                            return null;
                        }
                        break;
                    default:
                        between = 0;
                }
                stack.push(between);
            }
        }
        between = stack.pop();
        if (between == (long) between) {
            return "" + (long) between;
        } else {
            return "" + between;
        }
    }

    private List doParseToList(String s) {
        List<StringBuilder> list = new ArrayList<>();
        list.add(new StringBuilder());
        for (int i = 0; i < s.length(); i++) {
            if (!isOperator(s.charAt(i))) {
                getLast(list).append(s.charAt(i));
            } else {
                list.add(new StringBuilder());
                getLast(list).append(s.charAt(i));
                list.add(new StringBuilder());
            }
        }

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).length() == 0) {
                list.remove(i);
                i--;
            }
        }

        return list;
    }
}
