package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.size() > 254) {
            throw new CannotBuildPyramidException();
        }

        for (Integer inputNumber : inputNumbers) {
            if (inputNumber == null) {
                throw new CannotBuildPyramidException();
            }
        }

        List<Integer> ranges = new ArrayList<>();
        int j = 1;
        for (int i = 1; i < 100; i += j) {
            ranges.add(i);
            j++;
        }

        int listSize = inputNumbers.size();
        int x = 0;
        int y = 0;
        for (int i = 0; i < ranges.size(); i++) {
            if (listSize <= ranges.get(i)) {
                x = i + 1;
                y = x * 2 - 1;
                break;
            }
        }

        Collections.sort(inputNumbers);
        Queue<Integer> queue = new LinkedList<>(inputNumbers);

        boolean[][] table = new boolean[x][y];
        int[][] toReturn = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (j = 0; j < y; j++) {
                if (i == 0) {
                    if (j == (y - 1) / 2) {
                        table[i][j] = true;
                        toReturn[i][j] = queue.poll();
                    }

                    continue;
                }
                if (!queue.isEmpty()) {
                    if (j != 0 && table[i - 1][j - 1] == true) {
                        table[i][j] = true;
                        toReturn[i][j] = queue.poll();
                    } else if (j != y - 1 && table[i - 1][j + 1] == true) {
                        table[i][j] = true;
                        toReturn[i][j] = queue.poll();
                    }
                }
            }

        }

        return toReturn;
    }
}
